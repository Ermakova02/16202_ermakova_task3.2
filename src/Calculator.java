import java.util.Map;
import java.util.HashMap;

public class Calculator {
    private ListNode l1;
    private int flag;
    private String command;
   // private Token t1;
    private Map<String, Double> s = new HashMap<String, Double>();
    public int GetFlag()
    {
        return flag;
    }
    public void ChangeMap(String str, double d)
    {
        s.put(str, d);
    }
    public Map GetMap()
    {
        return s;
    }
    public Command performCommand(String name)
    {
        flag=0;
        int l = name.length();
        if (l!=0) {
            if (l==1)
            {
                if (name.compareTo("+") == 0)
                    return new Addition();
                else if (name.compareTo("-") == 0)
                    return new Subtraction();
                else if (name.compareTo("*") == 0)
                    return new Multiplication();
                else if (name.compareTo("/") == 0)
                    return new Division();
                else if (name.compareTo("#") == 0)
                {
                    flag = 2;
                    return null;
                }

            }
            else if (l==4) {
                if (name.compareTo("EXIT") == 0) {
                    flag = 1;
                    return null;
                }
                if (name.compareTo("SQRT") == 0)
                    return new Sqrt();
            }
            else if (l==5) {
                if (name.compareTo("PRINT") == 0)
                    return new Print();
            }
            else if (l==3) {
                if (name.compareTo("POP") == 0)
                    return new Pop();
            }
            else if (l>=6) {
                if ((name.indexOf(' ') == 6) & ((name.lastIndexOf(' ') != -1) | (name.lastIndexOf(' ') != 6)) & (name.substring(0, 6).equals("DEFINE")))
                    return new Define();
                else if ((name.indexOf(' ') == 4) & (name.substring(0, 4).equals("PUSH")))
                    return new Push();
            }
        }
        return null;


    }
  //  Calculator()
}
