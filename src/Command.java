abstract class Command {
    public abstract void Do(String str, Calculator obj);
}
