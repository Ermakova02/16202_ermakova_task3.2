import java.io.*;
import java.lang.*;
import java.util.*;
public class Main {

    public static void ReadWrite(Scanner sc)
    {
        Calculator obj1 = new Calculator();
        Command obj2 = null;
        int fl;
        String str = sc.nextLine();
        while (true) {
            obj2 = obj1.performCommand(str);
            fl=obj1.GetFlag();
            if (fl == 1) return;
            else if (fl == 2) continue;
            else if (fl == 0) obj2.Do(str, obj1);
            str = sc.nextLine();
        }
       /* while (true) {
            String str = sc.nextLine();
            int pos;
            if (str.compareTo("EXIT") == 0) break;
            else if (str.charAt(0) == '#')
            {
                continue;
            }
            else if ((str.indexOf(' ')==6)&(str.lastIndexOf(' ')!=-1)&(str.substring(0,5)=="DEFINE"))
            {
                Define command = new Define(str);
                command.Do();
            }
            //obj.SetCommand(str);
            System.out.println(str);
        }
        */
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        if (args.length == 1) {
            try {
                in = new Scanner(new FileReader(args[0]));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        ReadWrite(in);

    }
}