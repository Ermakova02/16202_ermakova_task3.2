import java.util.*;
public class Print extends Command {
    public void Do(String str, Calculator obj)
    {
        Map<String, Double> s = new HashMap<String, Double>();
        s=obj.GetMap();
       Set<Map.Entry<String,Double>> set = s.entrySet();
        for (Map.Entry<String, Double> me : set)
        {
            System.out.println(me.getKey() + " " + me.getValue());
        }
    }
}
